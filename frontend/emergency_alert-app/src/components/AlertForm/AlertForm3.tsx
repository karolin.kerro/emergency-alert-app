import React, { useState } from 'react';
import './AlertForm.css';

export default function Contact() {
  const [phoneNumber, setPhoneNumber] = useState('');
  const [name, setName] = useState('');

  const handleNameChange = (e) => {
    setName(e.target.value); // Update the name state when the input value changes
  };

  const handlePhoneNumberChange = (e) => {
    setPhoneNumber(e.target.value); // Update the phone number state when the input value changes
  };

  return (
    <div className="container">

      <h1 className="number">3</h1>
      <h1 className="title">Kontaktandmed</h1>

      <div className="row mb-3">
        <div className="col-md-12">
          <div className="mb-3 text-left">
            <label htmlFor="name" className="form-label">Ees ja perekonnanimi</label>
            <input
              id="name"
              className="form-control"
              placeholder="Sisesta nimi"
              value={name}
              onChange={handleNameChange}
            />
          </div>
        </div>

        <div className="col-md-12">
          <div className="mb-3 text-left">
            <label htmlFor="phoneNumber" className="form-label">
              Telefoninumber
            </label>
            <input
              type="tel"
              id="phone"
              className="form-control"
              placeholder="Sisesta telefoninumber"
              pattern="^(?:\+372|00372|8)[0-9]{7,8}$"
              value={phoneNumber}
              onChange={handlePhoneNumberChange}
            />
          </div>
            </div>
        </div>
        <div className="row mb-3">
        <div className="col-md-12">
          <div className="mb-3">
            <button className="btn btn-success btn-block">EDASI</button>
          </div>
        </div>
      </div>
    </div>
  );
}
