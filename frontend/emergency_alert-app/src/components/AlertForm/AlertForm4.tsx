import React, { useState } from 'react';
import './AlertForm.css';

export default function WhereHappened() {

  return (
    <div className="container">
      <h1 className="title">Kokkuvõte</h1>

      <div className="row mb-3">
        <div className="col-md-12">
          <div className="mb-3 text-left">
            <label htmlFor="where" className="form-label">
              Olukorra kirjeldus:
            </label>
            <input
              type="text"
              id="where"
              className="form-control"
              placeholder="Kuidas ligi pääseda / mõni maamärk vms"
            />
          </div>
        </div>
      </div>

      <div className="row mb-3">
        <div className="col-md-12">
          <div className="mb-3">
            <button className="btn btn-success btn-block">SAADA ÄRA</button>
          </div>
        </div>
      </div>
    </div>
  );
}
