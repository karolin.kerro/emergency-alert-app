import React from 'react'
import './AlertForm.css'
import { useState } from 'react';


export default function whatHappened() {
    const [whatHappened, setWhatHappened] = useState('');
    const [who, setWho] = useState('');
    const [image, setImage] = useState(null);

    const handleWhatHappenedChange = (e) => {
        setWhatHappened(e.target.value);
    };

    const handleWhoChange = (e) => {
        setWho(e.target.value);
    };

    const handleImageChange= (e) => {
        const selectedImage = e.target.files[0];
        setImage(selectedImaged);
    };

    function uploadFileToS3(arg0: string, image: null): void {
        throw new Error('Function not implemented.');
    }

  return (
  <div className="container">

    <h1 className='number'>1</h1>
    <h1 className="title">Mis juhtus?</h1>

    <div className="row mb-3">
      <div className="col-md-12">
        <div className="mb-3 text-left">
          <label htmlFor="whatHappened" className="form-label">
            Olukorra kirjeldus
          </label>
          <textarea
            id="whatHappened"
            className="form-control"
            placeholder= "Võimalikult täpne kirjeldus"
            value={whatHappened}
            onChange={handleWhatHappenedChange}
            rows={5}
          />
        </div>
      </div>
      <div className="col-md-12">
        <div className="mb-3  text-left">
          <label htmlFor="who" className="form-label">
            Kellega juhtus?
          </label>
          <input
            type="text"
            id="who"
            className="form-control"
            placeholder="Kirjuta liigi nimi või kirjeldus"
            value={who}
            onChange={handleWhoChange}
          />
        </div>
      </div>
    </div>
    <div className="row mb-3">
      <div className="col-md-12">
        <div className="mb-3 text-left">
          <label htmlFor="image" className="form-label">
            Pildid
          </label>
          <input
            type="file"
            id="image"
            className="form-control"
            accept="image/*"
            onChange={handleImageChange}
            placeholder="Lisa foto"
          />
        </div>
      </div>
      <div className="col-md-12">
        <div className="mb-3">
          {/* Center-align the button */}
          <button
            className="btn btn-success btn-block"
            onClick={() => uploadFileToS3('file', image)}
          >
            EDASI
          </button>
        </div>
      </div>
    </div>
  </div>
  );
} 