import React, { useState } from 'react';
import './AlertForm.css';

export default function WhereHappened() {
  const [where, setWhere] = useState(''); // Initialize state

  const handleWhereChange = (e) => {
    setWhere(e.target.value); // Update the state when the input value changes
  };

  return (
    <div className="container">
      <h1 className="number">2</h1>
      <h1 className="title">Kus juhtus?</h1>

      <div className="row mb-3">
        <div className="col-md-12">
          <div className="mb-3 text-left">
            <label className="form-label">Vali kaardil</label>
            <p className="map-details">
              Liiguta sõrmega kaarti, kuni punane täpp on õigel kohal.
            </p>
          </div>
        </div>

        <div className="col-md-12">
          <div className="mb-3 text-left">
            <label htmlFor="where" className="form-label">
              Juhised:
            </label>
            <input
              type="text"
              id="where"
              className="form-control"
              placeholder="Kuidas ligi pääseda / mõni maamärk vms"
              value={where}
              onChange={handleWhereChange}
            />
          </div>
        </div>
      </div>

      <div className="row mb-3">
        <div className="col-md-12">
          <div className="mb-3">
            <button className="btn btn-success btn-block">EDASI</button>
          </div>
        </div>
      </div>
    </div>
  );
}
