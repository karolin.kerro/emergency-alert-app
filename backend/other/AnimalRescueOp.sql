CREATE TABLE IF NOT EXISTS "users" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "firstname" varchar(50) NOT NULL,
  "lastname" varchar(50) NOT NULL,
  "birthdate" date NOT NULL,
  "phonenr" varchar(15) NOT NULL,
  "street" varchar(50) NOT NULL,
  "city" varchar(50) NOT NULL,
  "state" varchar(50) NOT NULL,
  "postalcode" varchar(5) NOT NULL,
  "email" varchar(50) UNIQUE NOT NULL,
  "role" varchar(50) NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT (now())
);

CREATE TABLE IF NOT EXISTS "tickets" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "status" varcar(10) NOT NULL,
  "young_animal" boolean,
  "open_date" date,
  "close_date" date,
  "description" varchar(1000),
  "location" varchar(50),
  "transport_possibility" boolean,
  "reporter_can_wait" boolean,
  "reporter_name" varchar(50),
  "reporter_phone" varchar(15),
  "created_at" timestamp,
  "foreign" boolean,
  "vet_appointment" boolean,
  "notification_source" varchar(50),
  "injury_type" varchar(10)
);

CREATE TABLE IF NOT EXISTS "images" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "ticket_id" integer
);

CREATE TABLE IF NOT EXISTS "videos" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "ticket_id" integer
);

CREATE TABLE IF NOT EXISTS "species" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "name" varchar(50),
  "exactspecies" varchar(50)
);

CREATE TABLE IF NOT EXISTS "tags" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "name" varchar(50)
);

CREATE TABLE IF NOT EXISTS "vetFiles" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "document" varchar(50),
  "ticket_id" uuid
);

CREATE TABLE IF NOT EXISTS "usersToTags" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "user_id" uuid,
  "tag_id" uuid
);

CREATE TABLE IF NOT EXISTS "ticketsToTags" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "ticket_id" uuid,
  "tag_id" uuid
);

CREATE TABLE IF NOT EXISTS "usersToSpecies" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "user_id" uuid,
  "species_id" uuid
);

CREATE TABLE IF NOT EXISTS "ticketsToSpecies" (
  "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
  "ticket_id" uuid,
  "species_id" uuid
);

ALTER TABLE "images" ADD FOREIGN KEY ("ticket_id") REFERENCES "tickets" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "videos" ADD FOREIGN KEY ("ticket_id") REFERENCES "tickets" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "usersToTags" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "usersToTags" ADD FOREIGN KEY ("tag_id") REFERENCES "tags" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "ticketsToTags" ADD FOREIGN KEY ("tag_id") REFERENCES "tags" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "ticketsToTags" ADD FOREIGN KEY ("ticket_id") REFERENCES "tickets" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "usersToSpecies" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "usersToSpecies" ADD FOREIGN KEY ("species_id") REFERENCES "species" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "ticketsToSpecies" ADD FOREIGN KEY ("ticket_id") REFERENCES "tickets" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "ticketsToSpecies" ADD FOREIGN KEY ("species_id") REFERENCES "species" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "vetFiles" ADD FOREIGN KEY ("ticket_id") REFERENCES "tickets" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
