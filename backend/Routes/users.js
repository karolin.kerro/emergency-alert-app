const express = require('express');
const router = express.Router();
const execute =  require("../startup/db/dao");

router.get("/", async (req, res) => {
    const users = await execute("SELECT * FROM users");
    if(users)  res.send(users);
    res.status(404).send()
})

router.post("/", async (req, res) => {
    console.log("post req recived");
    //console.log(req.body.firstname, req.body.lastname, req.body.email)
    const user = await execute("INSERT INTO users(firstname, lastname, email) values ($1, $2, $3) RETURNING*", [req.body.firstname, req.body.lastname, req.body.email]);
    res.send(user);
})


module.exports = router;