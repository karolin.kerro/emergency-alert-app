const express = require('express');
const router = express.Router();
const execute =  require("../startup/db/dao");


router.get("/", async (req, res) => {
    console.log("get request recieved")
    const tickets = await execute("SELECT * FROM tickets");
    if(tickets) res.send(tickets);
    res.status(500).send()
});


//Endpoint to save a new ticket into the database
router.post("/", async (req, res) => {
    console.log("post req recived");

    //TODO validate req.body json object
    //Do we need seperate validations?



    //TODO uplod images to a folder using multer and save ref to that image in the images table



    // Get the params and create a new ticket
    
    const  {
        status,
        tags,
        initial_species,
        young_animal,
        open_date,
        close_date,
        description,
        location,
        transport_possibility,
        reporter_can_wait,
        reporter_name,
        reporter_phone,
        foreign,
        vet_appointment,
        notification_source,
        injury_type
    } = req.body;
    
    params = [  status,
        initial_species,
        young_animal,
        open_date,
        close_date,
        description,
        location,
        transport_possibility,
        reporter_can_wait,
        reporter_name,
        reporter_phone,
        foreign,
        vet_appointment,
        notification_source,
        injury_type]

    const ticket = await execute('INSERT INTO tickets VALUES (DEFAULT, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, DEFAULT) RETURNING *', params);

    //Link correct species object with the ticket by creating a new row to ticketsToSpecies


    const species_id = await execute("SELECT id FROM species WHERE exactspecies = $1", [initial_species]);

    //console.log(ticket.rows[0].id)
    //console.log(species_id.rows[0].id)

    const add_ticket_species ={
        text: 'INSERT INTO "ticketsToSpecies" VALUES (DEFAULT, $1, $2)',
        values: [ticket.rows[0].id, species_id.rows[0].id]
    } 
    await execute(add_ticket_species); //NB kaheldav kas on await vaja aga testimiseks hea

    if(tags) {
        console.log("linking tags")
        //Link correct tags objects with the ticket by creating a new row to ticketsToTags
        tags.forEach(tag => {
            console.log(tag)
            execute("SELECT id FROM tags WHERE name = $1", [tag])
            .then((id) => {
                console.log("ticket ID:  ", ticket.rows[0].id)
                console.log("tag ID:    ", id.rows[0].id)
                execute('INSERT INTO "ticketsToTags" VALUES (DEFAULT,$1,$2)', [ticket.rows[0].id, id.rows[0].id])}).then(res => console.log(res))
    });
    }

    //lisab testimisel natuke aega, et andmebaasi saaks asjad salvestatud
    if(process.env.NODE_ENV == "test"){
        await new Promise((resolve, reject) => {
            setTimeout(() => {resolve("resolved")}, 30)
        })
    }



    res.send(ticket);
});


//endpoint to save a post to the db without knowing species and without tags (from ticket created through form)
router.post("/form", async (req, res) => {
    console.log("form post req recived");

    //TODO validate req.body json object
    //Do we need seperate validations?



     //TODO uplod images to a folder using multer and save ref to that image in the images table


    // Get the params and create a new ticket
    
    const  {
        status,
        initial_species,
        young_animal,
        open_date,
        close_date,
        description,
        location,
        transport_possibility,
        reporter_can_wait,
        reporter_name,
        reporter_phone,
        foreign,
        vet_appointment,
        notification_source,
        injury_type
    } = req.body;
    
    params = [  status,
        initial_species,
        young_animal,
        open_date,
        close_date,
        description,
        location,
        transport_possibility,
        reporter_can_wait,
        reporter_name,
        reporter_phone,
        foreign,
        vet_appointment,
        notification_source,
        injury_type]

    const user = await execute('INSERT INTO tickets VALUES (DEFAULT, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, DEFAULT) RETURNING *', params);
    if (user)  res.send(user);
    res.status(500).send()
});



module.exports = router;