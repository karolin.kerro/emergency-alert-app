const Pool = require("pg").Pool;
const config = require("config");


//databse connection settings NB: dont store your passwords here!
const pool = new Pool({
    user: "postgres",
    password: config.get("db-password"),
    database: config.get("db"),
    host: "localhost",
    port: "5432"
}); 

console.log(`connected to db:  ${config.get("db")}`)

//Executes a query
const execute = async(query, params, callback) => {
    try {
        //await pool.connect(); // create a connection
        const res = await pool.query(query, params, callback); // executes a query
        return res;
    } catch (error) {
        console.error(error);
        return false;
    }
};

module.exports = execute;