const execute = require("./dao");

// Create the required tabels if nessecery

async function creatTable(command, name){
    return await execute(command)
}

module.exports = function() {
    
    const createUsersTable = 
    `CREATE TABLE IF NOT EXISTS "users" (
        "id" uuid PRIMARY KEY DEFAULT gen_random_uuid(),
        "firstname" varchar(50) NOT NULL,
        "lastname" varchar(50) NOT NULL,
        "birthdate" date,
        "phonenr" varchar(15),
        "street" varchar(50),
        "city" varchar(50),
        "state" varchar(50),
        "postalcode" varchar(5),
        "email" varchar(50) UNIQUE NOT NULL,
        "role" varchar(50),
        "created_at" timestamp DEFAULT now()
    );`;

    const createTicketsTable =
    `CREATE TABLE IF NOT EXISTS "tickets" (
        "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
        "status" varchar(10) NOT NULL,
        "initial_species" varchar(50),
        "young_animal" boolean,
        "open_date" date,
        "close_date" date,
        "description" varchar(1000),
        "location" varchar(50),
        "transport_possibility" boolean,
        "reporter_can_wait" boolean,
        "reporter_name" varchar(50),
        "reporter_phone" varchar(15),
        "foreign" boolean,
        "vet_appointment" boolean,
        "notification_source" varchar(50),
        "injury_type" varchar(10),
        "created_at" timestamp
    );`
    
    const createSpeciesTable =
    `CREATE TABLE IF NOT EXISTS "species" (
        "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
        "name" varchar(50),
        "exactspecies" varchar(50)
    );`

    const createTagsTable =
    `CREATE TABLE IF NOT EXISTS "tags" (
        "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
        "name" varchar(50)
    );`

    const createImagesTable = `
    CREATE TABLE IF NOT EXISTS "images" (
        "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
        "img_ref" varchar(50),
        "ticket_id" uuid
        );`

    const createVideosTable = `
    CREATE TABLE IF NOT EXISTS "videos" (
        "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
        "vid_ref" varchar(50),
        "ticket_id" uuid
        );`
    
    const createVetFilesTable = `
        CREATE TABLE IF NOT EXISTS "vetFiles" (
        "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
        "document_ref" varchar(50),
        "ticket_id" uuid
        );`

    const createUsersToTagsTable = 
    `CREATE TABLE IF NOT EXISTS "usersToTags" (
        "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
        "user_id" uuid,
        "tag_id" uuid
    );`
    
    const createTicketsToTagsTable = 
    `CREATE TABLE IF NOT EXISTS "ticketsToTags" (
        "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
        "ticket_id" uuid,
        "tag_id" uuid
    );`
    
    const createUsersToSpeciesTable = 
    `CREATE TABLE IF NOT EXISTS "usersToSpecies" (
        "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
        "user_id" uuid,
        "species_id" uuid
    );`
    
    const createTicketsToSpeciesTable = 
    `CREATE TABLE IF NOT EXISTS "ticketsToSpecies" (
        "id" uuid PRIMARY KEY DEFAULT (gen_random_uuid()),
        "ticket_id" uuid,
        "species_id" uuid
    );`


    const createRelationships = 
    `ALTER TABLE "usersToTags" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
    ALTER TABLE "usersToTags" ADD FOREIGN KEY ("tag_id") REFERENCES "tags" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
    ALTER TABLE "ticketsToTags" ADD FOREIGN KEY ("tag_id") REFERENCES "tags" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
    ALTER TABLE "ticketsToTags" ADD FOREIGN KEY ("ticket_id") REFERENCES "tickets" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
    ALTER TABLE "usersToSpecies" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
    ALTER TABLE "usersToSpecies" ADD FOREIGN KEY ("species_id") REFERENCES "species" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
    ALTER TABLE "ticketsToSpecies" ADD FOREIGN KEY ("ticket_id") REFERENCES "tickets" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
    ALTER TABLE "ticketsToSpecies" ADD FOREIGN KEY ("species_id") REFERENCES "species" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
    ALTER TABLE "vetFiles" ADD FOREIGN KEY ("ticket_id") REFERENCES "tickets" ("id") ON DELETE SET NULL ON UPDATE CASCADE;
    ALTER TABLE "images" ADD FOREIGN KEY ("ticket_id") REFERENCES "tickets" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
    ALTER TABLE "videos" ADD FOREIGN KEY ("ticket_id") REFERENCES "tickets" ("id") ON DELETE CASCADE ON UPDATE CASCADE;`


    //create tables

    
    Promise.all([
    creatTable(createUsersTable, "users"),
    creatTable(createTagsTable, "tags"),
    creatTable(createTicketsTable, "tickets"),
    creatTable(createSpeciesTable, "species"),
    creatTable(createImagesTable, "images"),
    creatTable(createVideosTable, "videos"),
    creatTable(createVetFilesTable, "vetFiles"),
    creatTable(createUsersToSpeciesTable, "usersToSpecies"),
    creatTable(createUsersToTagsTable, "usersToTags"),
    creatTable(createTicketsToTagsTable, "ticketsToTags"),
    creatTable(createTicketsToSpeciesTable, "ticketsToSpecies")])
    .then(
    () => {
        console.log("nessecery tables created")
        execute(createRelationships).then(() => {
            console.log("relationships between tables created");
        }).catch(er => console.error(er));
        }
    ).catch((er) => {
        console.log("FATAL ERROR: some table was not created")
        process.exit(1);
    });
}