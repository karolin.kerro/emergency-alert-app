const config = require("config");

module.exports = function() {

    
    //console.log(config.get("db-password"));

    if(!config.get("db-password")) {

        console.error("FATAL ERROR: db-password cant be accessed, check env variables...");
        process.exit(1);
    };

}
