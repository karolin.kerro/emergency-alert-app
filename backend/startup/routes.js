const express = require("express");

//middleware
const error = require("../middleware/error")

//routes
const users = require("../Routes/users")
const tickets = require("../Routes/tickets");
const { json } = require("express");

module.exports = function(app) {

    app.use(express.json());

    app.get("/", (req, res) => { console.log("recieved"); res.status(200).send()})

    app.use("/api/tickets", tickets);
    app.use("/api/users", users);
    

    app.use(error);
    
}

