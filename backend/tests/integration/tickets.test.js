const request = require("supertest");
const execute = require("../../startup/db/dao")
let server;

describe("api/tickets", () => {
    beforeEach(() => {server = require("../../index")});
    afterEach(async () => {
        await server.close();
        await execute("TRUNCATE Tickets, Species CASCADE")
    });

    describe("GET /", () => {
        it("should return all tickets", async () => {

            const tickets = await execute("INSERT INTO Tickets VALUES (DEFAULT, $1, $2)", ["new", "siil"]);

            const res = await request(server).get("/api/tickets");
            //console.log(res.body)
            expect(res.status).toBe(200);
            expect(res.body.rows.length).toBe(1);
            expect(res.body.rows.some(g => g.initial_species === "siil")).toBeTruthy();
        });
    })

    describe("POST /form", () => {
        it("should save a ticket to database", async () => {
            const payload = {
                status: "new",
                initial_species: "siil"
            }

            const res = await request(server).post("/api/tickets/form").send(payload);
            const tickets = await execute("SELECT * FROM Tickets")

            expect(res.status).toBe(200)
            expect(tickets.rows.length).toBe(1)
            expect(tickets.rows.some(r => r.initial_species === "siil")).toBeTruthy()
        })
    })


    describe("POST /", () => {
        beforeEach(async () => {
            await execute("INSERT INTO species VALUES (DEFAULT, $1, $2)", ["siililised", "siil"])
        });

        it("should save a ticket to database", async () => {

            const payload = {
                status: "new",
                initial_species: "siil"
            }

            const res = await request(server).post("/api/tickets").send(payload);
            const tickets = await execute("SELECT * FROM Tickets")

            expect(res.status).toBe(200)
            expect(tickets.rows.length).toBe(1)
            expect(tickets.rows.some(r => r.status === "new")).toBeTruthy()
        })

        it("ticket should have species linked to it", async () => {
            const payload = {
                status: "new",
                initial_species: "siil"
            }
    
            const res = await request(server).post("/api/tickets").send(payload);
            const tickets = await execute("SELECT * FROM Tickets")
            const link = await execute('SELECT * FROM "ticketsToSpecies"')
    
            //console.log(link);
            //console.log(res.body)
    
            expect(res.status).toBe(200)
            expect(tickets.rows.length).toBe(1)
            expect(tickets.rows.some(r => r.status === "new")).toBeTruthy()
            expect(link.rows.length).toBe(1)
            expect(link.rows[0].ticket_id).toBe(res.body.rows[0].id)
        })

        it("ticket shoul have given tags linked to it", async () => {



            tags = await execute('INSERT INTO tags VALUES(DEFAULT, $1), (DEFAULT, $2)', ["kriitiline", "transport"])

            console.log(tags)


            const payload = {
                status: "new",
                initial_species: "siil",
                tags: ["transport", "kriitiline"]
            }
    
            const res = await request(server).post("/api/tickets").send(payload);
            const tickets = await execute("SELECT * FROM Tickets")
            const link = await execute('SELECT * FROM "ticketsToTags"')


    
            //console.log(link);
            //console.log(res.body)
    
            expect(res.status).toBe(200)
            expect(tickets.rows.length).toBe(1)
            expect(tickets.rows.some(r => r.status === "new")).toBeTruthy()
            expect(link.rows.length).toBe(2)
            expect(link.rows[0].ticket_id).toBe(res.body.rows[0].id)
        })
        

    })


}) //All tickets tests
