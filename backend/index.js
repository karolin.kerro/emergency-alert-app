const express = require("express");
const app = express();
const config = require("config");
const execute =  require("./startup/db/dao");



// CONFIG
require("./startup/config")();

//CONNECT TO DB
require("./startup/db/setup")();



//await setTimeout(() => {console.log("testing")}, 2000) // to let the tables be created before testing



//listening on PORT (default port 3000)
const PORT = config.get("PORT") || 3000;
const server = app.listen(PORT, () => console.log(`Listening on port: ${PORT} ... `))

//app.get("/", (req, res) => { console.log("recieved"); res.status(200)})

//ROUTES
require("./startup/routes")(app);


module.exports = server;


