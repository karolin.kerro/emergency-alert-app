# Emergency Alert App

The Wildlife Emergency Application is mobile and web application that seeks to provide a platform to collectively and cohesively coordinate the wildlife rescue efforts in Estonia. The main goal of the app will be to enable the public to easily and quickly notify the the Estonian Wildlife Center when they happen to notice an animal that may be in need of assistance or a check-up. By filling out a form, describing the situtation, a report will be sent to a central ticketing system, overseen by the volunteers at EWC (Estonian Wildlife Center), who can then respond appropriately and organize help to the animal in need.



## Link to wiki

- [ ] [Link to Wiki](https://gitlab.com/katariinaingerma/emergency-alert-app/-/wikis/home)



## Authors and acknowledgment
Holden Karl Hain
Lauri Lüüsi
Katariina Ingerma
Karolin Kerro

## License
For open source projects, say how it is licensed.

## Project status
In development
